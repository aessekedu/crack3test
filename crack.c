#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
FILE *openfile(char *fname);
char **readfile(FILE *f, int *pNumLines);
char **storeHashes(char **lines);
//Add a comment to this line
int tryguess(char *hash, char *guess);
int numLines;
int numHashes;
 
 int main(int argc, char *argv[])
 {
     
 
 // Read the hash file into an array of strings
    FILE *hf, *df;
    char **dictHashes;
	
	hf = openfile(argv[1]);
    char **hashes = readfile(hf, &numHashes);
    fclose(hf);
 
     // Read the dictionary file into an array of strings

    df = openfile(argv[2]);
    char **dict = readfile(df, &numLines);
    fclose(df);
    
    // Make array of hashes from the dictionary array
    dictHashes = storeHashes(dict);
    
     // For each hash, try every entry in the dictionary.
     // Print the matching dictionary entry.
   // Need two nested loops.
    for (int i = 0; i <= numHashes; i++)
    {	
    	for (int j = 0; j <= numLines; j++)
    	{
    	    if(strcmp(hashes[i], dictHashes[j]) == 0)
            {
    	    	printf("hash is: %s password is: %s\n", hashes[i], dict[j]);
    			break;
    		}	
    	}
    }
    
    int idx = 0;
    while(hashes[idx] != NULL)
    {
        free(hashes[idx]);
        idx++;
    }
    int idx2 = 0;
    while(dict[idx2] != NULL)
    {
        free(dict[idx2]);
        idx2++;
    }
    int idx3 = 0;
    while(dictHashes[idx3] != NULL)
    {
        free(dictHashes[idx3]);
        idx3++;
    }
    
    free(hashes);
    free(dict);
    free(dictHashes);
}

FILE *openfile(char *fname)
{
	FILE *f = fopen(fname, "r");
	if (!f)
	{
		printf("Couldn't open %s for reading\n", fname);
		exit(2);
	}
	return f;
}

// Read in a file and return the array of strings.
char **readfile(FILE *f, int *pNumLines)
{
    // Create inital array of 50 pointers
    int arrlen = 50;
    char **arr = (char **)malloc(arrlen * sizeof(char *));
    
    // Read in file, expanding array as you go
    char line[40];
    int i = 0;
    while(fscanf(f, "%s", line) != EOF)
    {
        arr[i] = malloc((strlen(line)+1) * sizeof(char));
        strcpy(arr[i], line);
        i++;
        if (i == arrlen)
        {
            // Make array 25% bigger
            arrlen = (int)(arrlen * 1.25);
            char **newarr = realloc(arr, arrlen * sizeof(char *));
            if (newarr != NULL) arr = newarr;
            else exit(3);
        }
    }
    *pNumLines = i - 1;
    return arr;
}

// Read in an array and return another array of hash strings of the first array.
char **storeHashes(char **lines)
{
    char **pwdHashes = (char **)malloc((numLines+1) * sizeof(char *));
    for (int i = 0; i < numLines; i++)
    {
        char *calchash;
        calchash = md5(lines[i], strlen(lines[i]));
        pwdHashes[i] = malloc((strlen(calchash)+1) * sizeof(char));
        strcpy(pwdHashes[i], calchash);
        free(calchash);
    }
   return pwdHashes;
 }